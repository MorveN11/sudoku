package finalproject.functions.constants;

public enum GameState {
    COMPLETE,
    ACTIVE,
    NEW
}
