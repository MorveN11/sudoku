package finalproject.functions.constants;

public class AppConstants {
  public final static int EMPTY_SPACES = 40;
  public static final int MAX_VALUE = 9;
  public static final String NAME_APPLICATION = "sudoku";
  public static final String NAME_SUDOKU = "ald-03";
}
