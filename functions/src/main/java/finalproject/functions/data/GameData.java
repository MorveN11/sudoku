package finalproject.functions.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import finalproject.functions.records.Sudoku;

public class GameData {
  private static final File GAME = new File("sudoku.txt");

  public static boolean createGame() {
    try {
      return GAME.createNewFile();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static boolean deleteGame() {
    return GAME.delete();
  }

  public static void updateGame(Sudoku game) {
    try {
      ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(GAME));
      objectOutputStream.writeObject(game);
      objectOutputStream.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static Sudoku getGame() {
    try {
      ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(GAME));
      Sudoku game = (Sudoku) objectInputStream.readObject();
      objectInputStream.close();
      return game;
    } catch (ClassNotFoundException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static boolean exists() {
    return GAME.exists();
  }
}
