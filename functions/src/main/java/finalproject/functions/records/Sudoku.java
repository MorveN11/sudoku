package finalproject.functions.records;

import finalproject.functions.constants.GameState;
import java.io.Serializable;
import java.util.HashMap;


public record Sudoku(GameState gameState, HashMap<Coordinates, Annotation> gameMap) implements Serializable {

}
