package finalproject.functions.records;

import finalproject.functions.constants.State;
import java.io.Serializable;

public record Annotation(String value, State state) implements Serializable {

}
