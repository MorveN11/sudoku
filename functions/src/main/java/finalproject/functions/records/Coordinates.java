package finalproject.functions.records;

import java.io.Serializable;

public record Coordinates(int x, int y) implements Serializable {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Coordinates other = (Coordinates) o;
        return this.x == other.x && this.y == other.y;
    }
}
