package finalproject.functions.logic;

import finalproject.functions.constants.GameState;
import finalproject.functions.constants.Rows;
import finalproject.functions.records.Annotation;
import finalproject.functions.records.Coordinates;
import finalproject.functions.records.Sudoku;
import java.util.*;

import static finalproject.functions.constants.AppConstants.MAX_VALUE;

public class Game {
    public static Sudoku getNewGame() {
        return new Sudoku(
                GameState.NEW,
                Generator.getNewGame()
        );
    }

    public static GameState checkForCompletion(HashMap<Coordinates, Annotation> sudoku) {
        var nativeGame = Utilities.convertToNative(sudoku);
        if (sudokuIsInvalid(nativeGame)) return GameState.ACTIVE;
        if (tilesAreNotFilled(nativeGame)) return GameState.ACTIVE;
        return GameState.COMPLETE;
    }

    public static boolean tilesAreNotFilled(int[][] sudoku) {
        for (int xIndex = 0; xIndex < MAX_VALUE; xIndex++) {
            for (int yIndex = 0; yIndex < MAX_VALUE; yIndex++) {
                if (sudoku[xIndex][yIndex] == 0) return true;
            }
        }
        return false;
    }

    public static boolean sudokuIsInvalid(int[][] sudoku) {
        if (rowsAreInvalid(sudoku)) return true;
        if (columnsAreInvalid(sudoku)) return true;
        return squaresAreInvalid(sudoku);
    }


    public static boolean squaresAreInvalid(int[][] sudoku) {
        if (rowOfSquaresIsInvalid(Rows.TOP, sudoku)) return true;
        if (rowOfSquaresIsInvalid(Rows.MIDDLE, sudoku)) return true;
        return rowOfSquaresIsInvalid(Rows.BOTTOM, sudoku);
    }

    private static boolean rowOfSquaresIsInvalid(Rows value, int[][] sudoku) {
        switch (value) {
            case TOP:
                if (squareIsInvalid(0, 0, sudoku)) return true;
                if (squareIsInvalid(0, 3, sudoku)) return true;
                return squareIsInvalid(0, 6, sudoku);
            case MIDDLE:
                if (squareIsInvalid(3, 0, sudoku)) return true;
                if (squareIsInvalid(3, 3, sudoku)) return true;
                return squareIsInvalid(3, 6, sudoku);

            case BOTTOM:
                if (squareIsInvalid(6, 0, sudoku)) return true;
                if (squareIsInvalid(6, 3, sudoku)) return true;
                return squareIsInvalid(6, 6, sudoku);

            default:
                return false;
        }
    }

    public static boolean squareIsInvalid(int yIndex, int xIndex, int[][] sudoku) {
        int yIndexEnd = yIndex + 3;
        int xIndexEnd = xIndex + 3;
        List<Integer> square = new ArrayList<>();
        while (yIndex < yIndexEnd) {
            while (xIndex < xIndexEnd) {
                square.add(
                        sudoku[xIndex][yIndex]
                );
                xIndex++;
            }
            xIndex -= 3;
            yIndex++;
        }
        return hasRepeated(square);
    }

    public static boolean columnsAreInvalid(int[][] sudoku) {
        for (int xIndex = 0; xIndex < MAX_VALUE; xIndex++) {
            List<Integer> row = new ArrayList<>();
            for (int yIndex = 0; yIndex < MAX_VALUE; yIndex++) {
                row.add(sudoku[xIndex][yIndex]);
            }
            if (hasRepeated(row)) return true;
        }
        return false;
    }

    public static boolean rowsAreInvalid(int[][] sudoku) {
        for (int yIndex = 0; yIndex < MAX_VALUE; yIndex++) {
            List<Integer> row = new ArrayList<>();
            for (int xIndex = 0; xIndex < MAX_VALUE; xIndex++) {
                row.add(sudoku[xIndex][yIndex]);
            }
            if (hasRepeated(row)) return true;
        }
        return false;
    }

    private static boolean hasRepeated(List<Integer> list) {
        return list.stream().filter(n -> n > 0).distinct()
                .anyMatch(n -> Collections.frequency(list, n) > 1);
    }
}
