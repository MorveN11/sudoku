package finalproject.functions.logic;

import finalproject.functions.constants.State;
import finalproject.functions.records.Annotation;
import finalproject.functions.records.Coordinates;
import java.util.HashMap;
import java.util.Map;

import static finalproject.functions.constants.AppConstants.MAX_VALUE;

public class Utilities {
    public static HashMap<Coordinates, Annotation> convertToHashMap(int[][] unsolvedGame) {
        HashMap<Coordinates, Annotation> map = new HashMap<>();
        for (int i = 0; i < unsolvedGame.length; i++) {
            for (int j = 0; j < unsolvedGame.length; j++) {
                if (unsolvedGame[i][j] == 0) {
                    map.put(new Coordinates(i, j), new Annotation("", State.OPTIONAL));
                } else {
                    map.put(new Coordinates(i, j),
                            new Annotation(String.valueOf(unsolvedGame[i][j]), State.FIXED));
                }
            }
        }
        return map;
    }

    public static int[][] convertToNative(HashMap<Coordinates, Annotation> sudoku) {
        int[][] matrix = new int[9][9];
        for (Map.Entry<Coordinates, Annotation> map : sudoku.entrySet()) {
            if (map.getValue().value().equals("")) {
                matrix[map.getKey().x()][map.getKey().y()] = 0;
            } else {
                matrix[map.getKey().x()][map.getKey().y()] = Integer.parseInt(map.getValue().value());
            }
        }
        return matrix;
    }

    public static void copySudokuArrayValues(int[][] oldArray, int[][] newArray) {
        for (int xIndex = 0; xIndex < MAX_VALUE; xIndex++){
            System.arraycopy(oldArray[xIndex], 0, newArray[xIndex], 0, MAX_VALUE);
        }
    }

    public static void clearArray(int[][] newGrid) {
        for (int xIndex = 0; xIndex < MAX_VALUE; xIndex++) {
            for (int yIndex = 0; yIndex < MAX_VALUE; yIndex++) {
                newGrid[xIndex][yIndex] = 0;
            }
        }
    }
}
