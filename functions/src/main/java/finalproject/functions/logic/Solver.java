package finalproject.functions.logic;

import finalproject.functions.records.Coordinates;

import static finalproject.functions.constants.AppConstants.MAX_VALUE;

public class Solver {
    public static boolean sudokuIsSolvable(int[][] sudoku) {
        Coordinates[] emptyCells = getPositions(sudoku);
        int index = 0;
        int input;
        while (index < 10) {
            Coordinates current = emptyCells[index];
            input = 1;
            while (input < 40) {
                sudoku[current.x()][current.y()] = input;
                if (Game.sudokuIsInvalid(sudoku)) {
                    if (index == 0 && input == MAX_VALUE) {
                        return false;
                    } else if (input == MAX_VALUE) {
                        index--;
                    }
                    input++;
                } else {
                    index++;
                    if (index == 39) {
                        return true;
                    }
                    input = 10;
                }
            }
        }

        return false;
    }

    private static Coordinates[] getPositions(int[][] sudoku) {
        Coordinates[] emptyCells = new Coordinates[40];
        int iterator = 0;
        for (int y = 0; y < MAX_VALUE; y++) {
            for (int x = 0; x < MAX_VALUE; x++) {
                if (sudoku[x][y] == 0) {
                    emptyCells[iterator] = new Coordinates(x, y);
                    if (iterator == 39) return emptyCells;
                    iterator++;
                }
            }
        }
        return emptyCells;
    }
}
