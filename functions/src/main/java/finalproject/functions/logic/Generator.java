package finalproject.functions.logic;

import finalproject.functions.records.Annotation;
import finalproject.functions.records.Coordinates;
import java.util.*;
import static finalproject.functions.constants.AppConstants.EMPTY_SPACES;
import static finalproject.functions.constants.AppConstants.MAX_VALUE;

public class Generator {
    public static HashMap<Coordinates, Annotation> getNewGame() {
        return Utilities.convertToHashMap(unsolvedGame(getSolvedGame()));
    }

    private static int[][] getSolvedGame() {
        Random random = new Random(System.currentTimeMillis());
        int[][] sudoku = new int[MAX_VALUE][MAX_VALUE];
        for (int value = 1; value <= MAX_VALUE; value++) {
            List<Coordinates> allocTracker = new ArrayList<>();
            int allocations = 0;
            int interrupt = 0;
            int attempts = 0;
            while (allocations < MAX_VALUE) {
                if (interrupt > 200) {
                    allocTracker.parallelStream().forEach(coord -> sudoku[coord.x()][coord.y()] = 0);
                    allocTracker.clear();
                    interrupt = 0;
                    allocations = 0;
                    attempts++;
                    if (attempts > 500) {
                        Utilities.clearArray(sudoku);
                        attempts = 0;
                        value = 1;
                    }
                }
                int x = random.nextInt(MAX_VALUE);
                int y = random.nextInt(MAX_VALUE);
                if (sudoku[x][y] == 0) {
                    sudoku[x][y] = value;
                    if (Game.sudokuIsInvalid(sudoku)) {
                        sudoku[x][y] = 0;
                        interrupt++;
                    } else {
                        allocTracker.add(new Coordinates(x, y));
                        allocations++;
                    }
                }
            }
        }
        return sudoku;
    }

    private static int[][] unsolvedGame(int[][] solvedGame) {
        Random random = new Random(System.currentTimeMillis());
        boolean solvable = false;
        int[][] solvableArray = new int[MAX_VALUE][MAX_VALUE];
        while (!solvable){
            Utilities.copySudokuArrayValues(solvedGame, solvableArray);
            int index = 0;
            while (index < EMPTY_SPACES) {
                int xCoordinate = random.nextInt(MAX_VALUE);
                int yCoordinate = random.nextInt(MAX_VALUE);
                if (solvableArray[xCoordinate][yCoordinate] != 0) {
                    solvableArray[xCoordinate][yCoordinate] = 0;
                    index++;
                }
            }
            int[][] toBeSolved = new int[MAX_VALUE][MAX_VALUE];
            Utilities.copySudokuArrayValues(solvableArray, toBeSolved);
            solvable = Solver.sudokuIsSolvable(toBeSolved);
        }
        return solvableArray;
    }
}
