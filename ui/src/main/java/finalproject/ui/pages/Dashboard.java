package finalproject.ui.pages;

import finalproject.ui.common.ComposableComponent;
import finalproject.ui.common.WindowComponent;
import finalproject.ui.molecules.TopBar;
import finalproject.ui.organisms.dashboard.play.DashboardPlayBody;
import finalproject.ui.organisms.dashboard.DashboardFooter;
import finalproject.ui.organisms.dashboard.DashboardHeader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class Dashboard implements ComposableComponent, WindowComponent {
  private final GridPane gridPane;
  private final Stage window;

  public Dashboard(Stage window) {
    this.gridPane = new GridPane();
    this.window = window;
  }

  @Override
  public void configureComponent() {
    gridPane.setVgap(66.5);
    gridPane.setAlignment(Pos.CENTER);
    gridPane.setPadding(new Insets(10));
    gridPane.setStyle("-fx-background-color: #141B28;");
    GridPane.setHgrow(gridPane, Priority.ALWAYS);
  }

  @Override
  public Node getComponent() {
    return gridPane;
  }

  @Override
  public void compose() {
    gridPane.add(new TopBar(getWindow(), getWidth()).build(), 0, 0);
    gridPane.add(new DashboardHeader().build(), 0, 1);
    gridPane.add(new DashboardPlayBody(window).build(), 0, 2);
    gridPane.add(new DashboardFooter().build(), 0, 3);
  }

  @Override
  public Stage getWindow() {
    return window;
  }

  @Override
  public Node getWindowComponent() {
    return this.build();
  }

  @Override
  public int getHeight() {
    return 900;
  }

  @Override
  public int getWidth() {
    return 1440;
  }
}
