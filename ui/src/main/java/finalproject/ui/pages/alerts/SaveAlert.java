package finalproject.ui.pages.alerts;

import finalproject.ui.common.ComposableComponent;
import finalproject.ui.common.WindowComponent;
import finalproject.ui.molecules.TopDeleteBar;
import finalproject.ui.organisms.alerts.save.SaveBody;
import finalproject.ui.organisms.alerts.save.SaveFooter;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class SaveAlert implements ComposableComponent, WindowComponent {
  private final GridPane pane;
  private final Stage window;

  public SaveAlert(Stage window) {
    pane = new GridPane();
    this.window = window;
  }

  @Override
  public void configureComponent() {
    pane.setAlignment(Pos.CENTER);
    pane.setPadding(new Insets(10));
    pane.setStyle("-fx-background-color: #141B28;");
    pane.setVgap(-150);
    GridPane.setHgrow(pane, Priority.ALWAYS);
  }

  @Override
  public void compose() {
    pane.addRow(0, new TopDeleteBar(getWindow(), getWidth()).build());
    pane.addRow(1, new SaveBody().build());
    pane.addRow(2, new SaveFooter(window).build());
  }

  @Override
  public Node getComponent() {
    return pane;
  }

  @Override
  public Stage getWindow() {
    return window;
  }

  @Override
  public Node getWindowComponent() {
    return this.build();
  }

  @Override
  public int getHeight() {
    return 667;
  }

  @Override
  public int getWidth() {
    return 1150;
  }
}
