package finalproject.ui.pages.alerts;

import finalproject.ui.common.ComposableComponent;
import finalproject.ui.common.WindowComponent;
import finalproject.ui.molecules.TopWinBar;
import finalproject.ui.organisms.alerts.win.WinBody;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class WinAlert implements ComposableComponent, WindowComponent {
  private final GridPane pane;
  private final Stage window;

  public WinAlert(Stage window) {
    pane = new GridPane();
    this.window = window;
  }

  @Override
  public void configureComponent() {
    pane.setAlignment(Pos.CENTER);
    pane.setPadding(new Insets(10));
    pane.setStyle("-fx-background-color: #141B28;");
    pane.setVgap(-50);
    GridPane.setHgrow(pane, Priority.ALWAYS);
  }

  @Override
  public void compose() {
    pane.addRow(0, new TopWinBar(getWindow(), getWidth()).build());
    pane.addRow(1, new WinBody().build());
  }

  @Override
  public Node getComponent() {
    return pane;
  }

  @Override
  public Stage getWindow() {
    return window;
  }

  @Override
  public Node getWindowComponent() {
    return this.build();
  }

  @Override
  public int getHeight() {
    return 575;
  }

  @Override
  public int getWidth() {
    return 1200;
  }
}
