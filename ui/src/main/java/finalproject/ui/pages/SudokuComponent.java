package finalproject.ui.pages;

import finalproject.functions.records.Sudoku;
import finalproject.ui.common.ComposableComponent;
import finalproject.ui.common.WindowComponent;
import finalproject.ui.molecules.TopSaveBar;
import finalproject.ui.organisms.sudoku.SudokuBody;
import finalproject.ui.organisms.sudoku.SudokuHeader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class SudokuComponent implements ComposableComponent, WindowComponent {
  private final GridPane gridPane;
  private final Stage window;
  private final Sudoku game;

  public SudokuComponent(Stage window, Sudoku game) {
    this.gridPane = new GridPane();
    this.window = window;
    this.game = game;
  }

  @Override
  public void configureComponent() {
    gridPane.setAlignment(Pos.CENTER);
    gridPane.setPadding(new Insets(10));
    gridPane.setStyle("-fx-background-color: #141B28;");
    gridPane.setVgap(3);
    GridPane.setHgrow(gridPane, Priority.ALWAYS);
  }

  @Override
  public Node getComponent() {
    return gridPane;
  }

  @Override
  public void compose() {
    gridPane.addRow(0, new TopSaveBar(window, getWidth()).build());
    gridPane.addRow(1, new SudokuHeader().build());
    gridPane.addRow(2, new SudokuBody(game, window).build());
  }

  @Override
  public Stage getWindow() {
    return window;
  }

  @Override
  public Node getWindowComponent() {
    return this.build();
  }

  @Override
  public int getHeight() {
    return 1000;
  }

  @Override
  public int getWidth() {
    return 1000;
  }
}
