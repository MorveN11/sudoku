package finalproject.ui;

import finalproject.functions.data.GameData;
import finalproject.ui.pages.Dashboard;
import finalproject.ui.pages.DashboardRestart;
import javafx.application.Application;
import javafx.stage.Stage;

public class AppApplication extends Application {
  @Override
  public void start(Stage primaryStage) {
    if (GameData.exists()) {
      new DashboardRestart(primaryStage).openWindow();
    } else {
      new Dashboard(primaryStage).openWindow();
    }
  }

  public static void main(String[] args) {
    launch();
  }
}
