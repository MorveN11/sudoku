package finalproject.ui.atoms.label;

import javafx.scene.control.Label;

public class AppLabel extends Label {
  public AppLabel(String text, int size) {
    setText(text);
    setStyle("-fx-font-size: " + size + ";");
    getStyleClass().add("app-label");
  }
}
