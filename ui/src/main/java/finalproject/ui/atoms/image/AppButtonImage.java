package finalproject.ui.atoms.image;

import javafx.scene.Cursor;
import javafx.scene.control.Button;

public class AppButtonImage extends Button {
  public AppButtonImage(String image) {
    setCursor(Cursor.HAND);
    setStyle("-fx-focus-color: transparent;");
    setStyle("-fx-background-color: transparent;");
    setGraphic(new AppImageView(image));
  }
}
