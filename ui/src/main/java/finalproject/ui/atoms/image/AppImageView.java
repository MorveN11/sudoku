package finalproject.ui.atoms.image;

import javafx.scene.image.ImageView;

public class AppImageView extends ImageView {
  public AppImageView(String image) {
    super(image);
  }
}
