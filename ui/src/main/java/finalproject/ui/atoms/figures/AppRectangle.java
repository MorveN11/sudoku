package finalproject.ui.atoms.figures;

import javafx.scene.shape.Rectangle;

public class AppRectangle extends Rectangle {
  public AppRectangle(int x, int y, int width, int height) {
    setX(x);
    setY(y);
    setWidth(width);
    setHeight(height);
    getStyleClass().add("app-rectangle");
  }
}
