package finalproject.ui.atoms.figures;

public class AppVerticalRectangle extends AppRectangle {
  public AppVerticalRectangle(int x, int large) {
    super(80 * x, 0, large, 720);
  }
}
