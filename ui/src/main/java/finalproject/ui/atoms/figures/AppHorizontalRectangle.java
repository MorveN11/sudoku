package finalproject.ui.atoms.figures;

public class AppHorizontalRectangle extends AppRectangle {
  public AppHorizontalRectangle(int y, int large) {
    super(0, 80 * y, 720, large);
  }
}
