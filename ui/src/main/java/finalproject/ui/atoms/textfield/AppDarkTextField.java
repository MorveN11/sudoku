package finalproject.ui.atoms.textfield;

import javafx.stage.Stage;

public class AppDarkTextField extends AppTextField {
  public AppDarkTextField(int x, int y, Stage window) {
    super(x, y, window);
    getStyleClass().add("dark-field");
  }
}
