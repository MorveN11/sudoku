package finalproject.ui.atoms.textfield;

import finalproject.functions.records.Annotation;
import finalproject.functions.logic.Game;
import finalproject.functions.constants.GameState;
import finalproject.functions.constants.State;
import finalproject.functions.data.GameData;
import finalproject.functions.records.Coordinates;
import finalproject.functions.records.Sudoku;
import finalproject.ui.pages.alerts.WinAlert;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AppTextField extends TextField {
    private static final int SIZE = 80;
    private final int x;
    private final int y;
    private final Stage window;

    public AppTextField(int x, int y, Stage window) {
        this.x = x;
        this.y = y;
        this.window = window;
        setLayoutX(y * SIZE);
        setLayoutY(x * SIZE);
        setOnKeyPressed(this::getTextFromEvent);
    }

    private void getTextFromEvent(KeyEvent event) {
        if (event.getEventType() == KeyEvent.KEY_PRESSED) {
            if (event.getText().matches("[1-9]") && !event.getText().equals(getText())) {
                updateGame(event.getText());
            }
        }
        event.consume();
    }

    private void updateGame(String value) {
        var map = GameData.getGame().gameMap();
        map.replace(new Coordinates(x, y), new Annotation(value, State.OPTIONAL));
        GameData.updateGame(new Sudoku(
                Game.checkForCompletion(map),
                map
        ));
        if (GameData.getGame().gameState() == GameState.COMPLETE) {
            new WinAlert(window).openNewWindow();
        }
    }

    @Override
    public void replaceText(int i, int i1, String s) {
        if (s.matches("[1-9]") && !s.equals(getText())) {
            super.textProperty().setValue(s);
        }
    }
}
