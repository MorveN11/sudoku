package finalproject.ui.atoms.textfield;

import javafx.stage.Stage;

public class AppLightTextField extends AppTextField {
  public AppLightTextField(int x, int y, Stage window) {
    super(x, y, window);
    getStyleClass().add("light-field");
  }
}
