package finalproject.ui.atoms.buttons;

import finalproject.functions.data.GameData;
import javafx.stage.Stage;

public class AppExitDeleteButton extends AppExitButton {
  public AppExitDeleteButton(Stage stage) {
    super(stage);
  }

  @Override
  public void buttonAction() {
    if (GameData.exists() && GameData.deleteGame()) {
      super.getStage().close();
    } else {
      super.getStage().close();
    }
  }
}
