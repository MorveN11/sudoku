package finalproject.ui.atoms.buttons;

import finalproject.ui.pages.alerts.SaveAlert;
import javafx.stage.Stage;

public class AppExitSaveButton extends AppExitButton {

  public AppExitSaveButton(Stage stage) {
    super(stage);
  }

  @Override
  public void buttonAction() {
    new SaveAlert(super.getStage()).openNewWindow();
  }
}
