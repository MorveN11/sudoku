package finalproject.ui.atoms.buttons.common;

import finalproject.ui.atoms.image.AppButtonImage;
import finalproject.ui.common.Component;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;

public interface IAppExit extends Component {

  AppButtonImage getButton();

  void buttonAction();

  @Override
  default Node getComponent() {
    return getButton();
  }

  @Override
  default void configureComponent() {
    getButton().setAlignment(Pos.CENTER_RIGHT);
    getButton().setPadding(new Insets(10, 30, 0, 0));
    getButton().setOnAction(e -> buttonAction());
  }
}
