package finalproject.ui.atoms.buttons;

import finalproject.functions.data.GameData;
import finalproject.ui.pages.Dashboard;
import javafx.stage.Stage;

public class AppExitWinButton extends AppExitButton{
  public AppExitWinButton(Stage stage) {
    super(stage);
  }

  @Override
  public void buttonAction() {
    if (GameData.exists() && GameData.deleteGame()) {
      new Dashboard(getStage()).openNewWindow();
    } else {
      new Dashboard(getStage()).openNewWindow();
    }
  }
}
