package finalproject.ui.atoms.buttons;

import finalproject.ui.atoms.image.AppButtonImage;
import finalproject.ui.atoms.buttons.common.IAppExit;
import javafx.stage.Stage;

public class AppExitButton implements IAppExit {
  private final AppButtonImage button;
  private final Stage stage;

  public AppExitButton(Stage stage) {
    this.stage = stage;
    button = new AppButtonImage("exit_icon.png");
  }

  public Stage getStage() {
    return stage;
  }

  @Override
  public AppButtonImage getButton() {
    return button;
  }

  @Override
  public void buttonAction() {
    stage.close();
  }
}
