package finalproject.ui.atoms.buttons;

import javafx.scene.Cursor;
import javafx.scene.control.Button;

public class AppButton extends Button {
  public AppButton(String text, int size) {
    setText(text);
    setCursor(Cursor.HAND);
    setStyle("-fx-font-size: " + size + ";");
    getStyleClass().add("app-button");
  }
}
