package finalproject.ui.molecules;

import finalproject.ui.atoms.buttons.AppExitButton;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class TopBar implements ComposableComponent {
  private final HBox container;
  private final AppExitButton button;
  private final int width;

  public TopBar(Stage stage, int width) {
    this.width = width;
    container = new HBox();
    button = new AppExitButton(stage);
  }

  @Override
  public void configureComponent() {
    container.setPrefWidth(this.width);
    container.setAlignment(Pos.CENTER_RIGHT);
  }

  @Override
  public void compose() {
    container.getChildren().add(button.build());
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
