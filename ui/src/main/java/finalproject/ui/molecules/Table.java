package finalproject.ui.molecules;

import finalproject.functions.records.Annotation;
import finalproject.functions.constants.State;
import finalproject.functions.records.Coordinates;
import finalproject.functions.records.Sudoku;
import finalproject.ui.atoms.figures.AppHorizontalRectangle;
import finalproject.ui.atoms.figures.AppVerticalRectangle;
import finalproject.ui.atoms.textfield.AppDarkTextField;
import finalproject.ui.atoms.textfield.AppLightTextField;
import finalproject.ui.atoms.textfield.AppTextField;
import finalproject.ui.common.Component;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.stage.Stage;
import java.util.HashMap;
import java.util.Map;

public class Table implements Component {
  private final Group pane;
  private final Sudoku game;
  private final Stage window;
  private final HashMap<Coordinates, AppTextField> map;

  public Table(Sudoku game, Stage window) {
    pane = new Group();
    map = new HashMap<>();
    this.game = game;
    this.window = window;
  }

  private void configureTextFields() {
    AppTextField field;
    for (int x = 0; x < 9; x++) {
      for (int y = 0; y < 9; y++) {
        if (x % 2 == 0 && y % 2 == 0) {
          field = new AppLightTextField(x, y, window);
        } else if (x % 2 == 0) {
          field = new AppDarkTextField(x, y, window);
        } else if (y % 2 == 0) {
          field = new AppDarkTextField(x, y, window);
        } else {
          field = new AppLightTextField(x, y, window);
        }
        map.put(new Coordinates(x, y), field);
        pane.getChildren().add(field);
      }
    }
  }

  private void configureGridLines() {
    int width;
    for (int i = 1; i < 9 ; i++) {
      width = i == 3 || i == 6 ? 6 : 3;
      pane.getChildren().addAll(
              new AppVerticalRectangle(i, width),
              new AppHorizontalRectangle(i, width)
      );
    }
  }

  private void addSudokuFields() {
    for (Map.Entry<Coordinates, Annotation> entry : game.gameMap().entrySet()) {
      if (entry.getValue().state() == State.FIXED) {
        map.get(new Coordinates(entry.getKey().x(), entry.getKey().y())).setDisable(true);
        map.get(new Coordinates(entry.getKey().x(), entry.getKey().y())).setOpacity(100);
        if (entry.getKey().x() % 2 == 0 && entry.getKey().y() % 2 == 0) {
          map.get(new Coordinates(entry.getKey().x(), entry.getKey().y())).getStyleClass().add("dark-fixed-field");
        } else if (entry.getKey().x() % 2 == 0) {
          map.get(new Coordinates(entry.getKey().x(), entry.getKey().y())).getStyleClass().add("light-fixed-field");
        } else if (entry.getKey().y() % 2 == 0) {
          map.get(new Coordinates(entry.getKey().x(), entry.getKey().y())).getStyleClass().add("light-fixed-field");
        } else {
          map.get(new Coordinates(entry.getKey().x(), entry.getKey().y())).getStyleClass().add("dark-fixed-field");
        }
      }
      map.get(new Coordinates(entry.getKey().x(), entry.getKey().y())).setText(entry.getValue().value());
    }
  }

  @Override
  public void configureComponent() {
    configureTextFields();
    configureGridLines();
    addSudokuFields();
  }

  @Override
  public Node getComponent() {
    return pane;
  }
}
