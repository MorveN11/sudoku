package finalproject.ui.molecules;

import finalproject.ui.atoms.buttons.AppExitDeleteButton;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class TopDeleteBar implements ComposableComponent {
  private final HBox container;
  private final AppExitDeleteButton button;
  private final int width;

  public TopDeleteBar(Stage stage, int width) {
    this.width = width;
    container = new HBox();
    button = new AppExitDeleteButton(stage);
  }

  @Override
  public void configureComponent() {
    container.setPrefWidth(this.width);
    container.setAlignment(Pos.CENTER_RIGHT);
  }

  @Override
  public void compose() {
    container.getChildren().add(button.build());
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
