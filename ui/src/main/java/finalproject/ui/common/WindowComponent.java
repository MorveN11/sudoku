package finalproject.ui.common;

import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public interface WindowComponent {
  Stage getWindow();

  Node getWindowComponent();

  int getWidth();

  int getHeight();

  default void openWindow() {
    Scene scene = new Scene(new StackPane(getWindowComponent()), getWidth(), getHeight());
    scene.getStylesheets().add("StyleSheet.css");
    getWindow().initStyle(StageStyle.UNDECORATED);
    getWindow().setScene(scene);
    getWindow().show();
  }

  default void openNewWindow() {
    getWindow().close();
    Scene scene = new Scene(new StackPane(getWindowComponent()), getWidth(), getHeight());
    scene.getStylesheets().add("StyleSheet.css");
    getWindow().setScene(scene);
    getWindow().show();
    Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
    getWindow().setX((screenBounds.getWidth() - getWindow().getWidth()) / 2);
    getWindow().setY((screenBounds.getHeight() - getWindow().getHeight()) / 2);
  }
}
