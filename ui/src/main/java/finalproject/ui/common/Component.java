package finalproject.ui.common;

import javafx.scene.Node;

public interface Component {
  void configureComponent();

  Node getComponent();

  default Node build() {
    configureComponent();
    return getComponent();
  }
}
