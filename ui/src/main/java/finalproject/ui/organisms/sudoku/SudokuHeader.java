package finalproject.ui.organisms.sudoku;

import finalproject.ui.atoms.label.AppLabel;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

import static finalproject.functions.constants.AppConstants.NAME_SUDOKU;

public class SudokuHeader implements ComposableComponent {
  private final HBox container;

  public SudokuHeader() {
    container = new HBox();
  }


  @Override
  public void configureComponent() {
    container.setAlignment(Pos.CENTER);
    container.setPrefWidth(1000);
  }

  @Override
  public void compose() {
    container.getChildren().add(new AppLabel(NAME_SUDOKU, 64));
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
