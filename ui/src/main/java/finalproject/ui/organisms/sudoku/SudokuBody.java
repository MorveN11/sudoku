package finalproject.ui.organisms.sudoku;

import finalproject.functions.records.Sudoku;
import finalproject.ui.common.ComposableComponent;
import finalproject.ui.molecules.Table;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class SudokuBody implements ComposableComponent {
  private final HBox container;
  private final Sudoku game;
  private final Stage window;

  public SudokuBody(Sudoku game, Stage window) {
    container = new HBox();
    this.game = game;
    this.window = window;
  }


  @Override
  public void configureComponent() {
    container.setAlignment(Pos.CENTER);
    container.setPrefWidth(1000);
    container.setPrefHeight(800);
  }

  @Override
  public void compose() {
    container.getChildren().add(new Table(game, window).build());
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
