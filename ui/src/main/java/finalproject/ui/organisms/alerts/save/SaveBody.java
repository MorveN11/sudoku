package finalproject.ui.organisms.alerts.save;

import finalproject.ui.atoms.image.AppImageView;
import finalproject.ui.atoms.label.AppLabel;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

public class SaveBody implements ComposableComponent {
  private final Group container;
  private final VBox text;
  private final AppImageView image;

  public SaveBody() {
    container = new Group();
    text = new VBox();
    image = new AppImageView("warning.png");
  }


  @Override
  public void configureComponent() {
    text.setAlignment(Pos.CENTER);
    text.setPrefWidth(1100);
    text.setPrefHeight(300);
    text.setSpacing(10);
    text.setLayoutY(250);
    image.setLayoutX(235);
    image.setLayoutY(50);
  }

  @Override
  public void compose() {
    text.getChildren().addAll(
            new AppLabel("Do you want to save", 72),
            new AppLabel("the game?", 72)
    );
    container.getChildren().addAll(image, text);
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
