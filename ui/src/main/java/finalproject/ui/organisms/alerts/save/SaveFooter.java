package finalproject.ui.organisms.alerts.save;

import finalproject.functions.data.GameData;
import finalproject.ui.atoms.buttons.AppButton;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class SaveFooter implements ComposableComponent {
  private final HBox container;
  private final AppButton noButton;
  private final AppButton yesButton;
  private final Stage window;

  public SaveFooter(Stage window) {
    container = new HBox();
    noButton =  new AppButton("No", 46);
    yesButton = new AppButton("Yes", 46);
    this.window = window;
  }

  private void noButtonPressed() {
    if (GameData.exists() && GameData.deleteGame()) {
      window.close();
    } else {
      window.close();
    }
  }

  private void yesButtonPressed() {
    if (!GameData.exists() && GameData.createGame()) {
      GameData.updateGame(GameData.getGame());
      window.close();
    } else if (GameData.exists()) {
      GameData.updateGame(GameData.getGame());
      window.close();
    }
  }

  @Override
  public void configureComponent() {
    container.setAlignment(Pos.CENTER);
    container.setPrefWidth(1100);
    container.setPrefHeight(200);
    container.setSpacing(150);
    noButton.setOnAction(e -> noButtonPressed());
    yesButton.setOnAction(e -> yesButtonPressed());
  }

  @Override
  public void compose() {
    container.getChildren().addAll(
            noButton,
            yesButton
    );
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
