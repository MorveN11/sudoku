package finalproject.ui.organisms.alerts.win;

import finalproject.ui.atoms.image.AppImageView;
import finalproject.ui.atoms.label.AppLabel;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

public class WinBody implements ComposableComponent {
  private final Group container;
  private final VBox text;
  private final AppImageView image;

  public WinBody() {
    container = new Group();
    text = new VBox();
    image = new AppImageView("win.png");
  }


  @Override
  public void configureComponent() {
    text.setAlignment(Pos.CENTER);
    text.setPrefWidth(1200);
    text.setSpacing(10);
    text.setLayoutY(125);
    image.setLayoutX(375);
  }

  @Override
  public void compose() {
    text.getChildren().addAll(
            new AppLabel("Congratulations!!!", 96),
            new AppLabel("you won the game", 96)
    );
    container.getChildren().addAll(image, text);
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
