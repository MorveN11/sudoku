package finalproject.ui.organisms.dashboard;

import finalproject.ui.atoms.image.AppImageView;
import finalproject.ui.atoms.label.AppLabel;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static finalproject.functions.constants.AppConstants.NAME_APPLICATION;

public class DashboardHeader implements ComposableComponent{
  private final VBox container;

  public DashboardHeader() {
    container = new VBox();
  }

  @Override
  public void configureComponent() {
    container.setAlignment(Pos.CENTER);
    container.setPrefWidth(1440);
    container.setSpacing(-25);
  }

  @Override
  public void compose() {
    HBox images = new HBox(new AppImageView("sudoku_icon.png"),
            new AppImageView("mathematics_icon.png"));
    images.setSpacing(750);
    images.setAlignment(Pos.CENTER);
    container.getChildren().addAll(images, new AppLabel(NAME_APPLICATION, 128));
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
