package finalproject.ui.organisms.dashboard.play;

import finalproject.functions.data.GameData;
import finalproject.functions.logic.Game;
import finalproject.ui.atoms.buttons.AppButton;
import finalproject.ui.common.ComposableComponent;
import finalproject.ui.pages.SudokuComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DashboardPlayBody implements ComposableComponent {
  private final HBox container;
  private final AppButton playButton;
  private final Stage window;

  public DashboardPlayBody(Stage window) {
    container = new HBox();
    playButton = new AppButton("PLAY", 60);
    this.window = window;
  }

  private void playButtonPressed() {
    var game = Game.getNewGame();
    GameData.createGame();
    GameData.updateGame(game);
    new SudokuComponent(window, game).openNewWindow();
  }

  @Override
  public void configureComponent() {
    container.setAlignment(Pos.CENTER);
    container.setPrefWidth(1440);
    playButton.setOnAction(event -> playButtonPressed());
  }

  @Override
  public void compose() {
    container.getChildren().add(playButton);
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
