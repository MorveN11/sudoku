package finalproject.ui.organisms.dashboard;

import finalproject.ui.atoms.label.AppLabel;
import finalproject.ui.common.ComposableComponent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

public class DashboardFooter implements ComposableComponent {
  private final HBox container;

  public DashboardFooter() {
    container = new HBox();
  }

  @Override
  public void configureComponent() {
    container.setAlignment(Pos.CENTER_RIGHT);
    container.setPrefWidth(1440);
    container.setPadding(new Insets(25, 75, 15, 0));
  }

  @Override
  public void compose() {
    container.getChildren().add(
            new AppLabel("© 2022 ALD-03", 40)
    );
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
