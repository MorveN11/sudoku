package finalproject.ui.organisms.dashboard.restart;

import finalproject.functions.logic.Game;
import finalproject.functions.data.GameData;
import finalproject.functions.records.Sudoku;
import finalproject.ui.atoms.buttons.AppButton;
import finalproject.ui.common.ComposableComponent;
import finalproject.ui.pages.SudokuComponent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DashboardRestartBody implements ComposableComponent {
  private final HBox container;
  private final AppButton restartButton;
  private final AppButton continueButton;
  private final Stage window;

  public DashboardRestartBody(Stage window) {
    container = new HBox();
    container.setSpacing(250);
    restartButton = new AppButton("RESTART", 60);
    continueButton = new AppButton("CONTINUE", 60);
    this.window = window;
  }

  private void restartButtonPressed() {
    var game = Game.getNewGame();
    GameData.deleteGame();
    GameData.createGame();
    GameData.updateGame(game);
    new SudokuComponent(window, game).openNewWindow();
  }

  private void continueButtonPressed() {
    if (GameData.exists()) {
      new SudokuComponent(window,
              new Sudoku(GameData.getGame().gameState(), GameData.getGame().gameMap())).openNewWindow();
    } else {
      new SudokuComponent(window,
              new Sudoku(GameData.getGame().gameState(), GameData.getGame().gameMap())).openNewWindow();
    }
  }

  @Override
  public void configureComponent() {
    container.setAlignment(Pos.CENTER);
    container.setPrefWidth(1440);
    restartButton.setOnAction(event -> restartButtonPressed());
    continueButton.setOnAction(event -> continueButtonPressed());
  }

  @Override
  public void compose() {
    container.getChildren().addAll(restartButton, continueButton);
  }

  @Override
  public Node getComponent() {
    return container;
  }
}
